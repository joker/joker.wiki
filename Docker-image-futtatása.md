
A klasztreren a Sarus elnevezésű kontainer motor van telepítve, ami közvetlenül képes Docker image letöltésére és futatására. A letöltendő image közvetlenül elérhető a Docker Hub-ról, vagy más Docker image térolóról, esetleg tar fájlból. 
A Sarus Docker image készítésére, módosítására közvetlenül nem alkalmas ezt normál docker környzetben kell elvégezni, mivel a Sarus-t alapvetően produktív HPC környezetre tervezték. A rendszer dokumentációja részletesen leírja az egyes használati eseteket (https://sarus.readthedocs.io/en/stable/) itt csak rövid összefoglalót adunk a legfontosabb parancsokról. 

**Fontos**, hogy a Sarus környezeti változói be legyenek állítva, amint a `module load sarus` paraccsal tehetünk meg (ld. Belépés).

#### [Hello Sarus](Hello Sarus)

### [Interaktív futtatás](Interaktív futtatás)

### [GPU elérés konténerből](GPU elérés konténerből)
