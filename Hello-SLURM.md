Legyen a SLURM "Hello World" programja (*hello.sh*) egy olyan shell szkript, ami valamelyik workeren lefutva kiírja, hogy "Hello SLURM from hostname", és vár 20 másodpercet, hogy ne azonnal érjen véget a szkript:
```
#!/bin/bash
# hello.sh
# Egyszerű slurm példa
echo "Hello SLURM from: " `/usr/bin/hostname`
sleep 20
```
Fontos, hogy a script végrehatható legyen (`chmod +x hello.sh`)! Indítsuk el ezt is az srun paranccsal 5 párhuzamos példányban a debug partícióban: 
```
$ srun -p debug -n5 hello.sh
Hello SLURM from:  cn10
Hello SLURM from:  cn10
Hello SLURM from:  cn10
Hello SLURM from:  cn10
Hello SLURM from:  cn10
```

Amennyiben nincs elegendő erőforrás szabadon, az *srun* parancs várakozni fog az erőforrások felszabadulására. Ezt általában nem akarjuk megvárni, ezért leggyakrabban az `sbatch` parancsot használjuk, amivel batch szkriptet tehetünk a várakozó sorba. Ez a script akkor fog elindulni, amikor minden olyan erőforrás felszabadul, ami a futáshoz szükséges. Példaként legyen egy run.sh szrikptünk: 
```
#!/bin/bash 
# run.sh 
# egyszerű szkript az sbatch-hoz.
srun $@
```
  
Indítsuk el most az `sbatch -p debug -n5 ./hello.sh` paranccsal a jól bevált hello.sh szkriptünket! Eredményül kiíródik a job azonosítója pl: 
```
Submitted batch job 339
```

Az `squeue` paraccsal ellenőrizhetjük job állapotát, melynek eredménye ehhez hasonló lehet: 
```
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
               339     debug   run.sh    szebi  R       0:04      1 cn10
```

A job eredményként egy `slurm-339.out` nevű fájl keletkezik az aktuális munkakatalógusban, ami tartalmazza a job standard kimenetét:
```
$ cat slurm-339.out
Hello SLURM from:  cn10
Hello SLURM from:  cn10
Hello SLURM from:  cn10
Hello SLURM from:  cn10
Hello SLURM from:  cn10
```

Fontos megjegyezni, hogy a *run.sh* szkriptre csak azért volt szükség, hogy párhuzamosan induljanak a jobok. Ha csak egy processzt kell indítani, mert nem párhuzamos a feladatat, vagy az az egy processz a felelős a többi processz indításáért (pl. mpirun), akkor a *hello.sh* közvetlenül betehető a várakozási sorba. Pl:
``` 
$ sbatch -p debug hello.sh
Submitted batch job 340
squeue # megvárjuk a futás végét
$ cat slurm-340
Hello SLURM from:  cn04
```
