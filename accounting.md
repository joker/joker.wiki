Részletes dokumentáció: 
 * [slurm accounting](https://slurm.schedmd.com/accounting.html)
 * [slurm resource limits](https://slurm.schedmd.com/resource_limits.html)
 * [trackable resources](https://slurm.schedmd.com/tres.html)

## Általános
A joker klaszteren a következő SLURM csoportokat határoztuk meg:
 * `root` - a root felhasználó csoportja
 * `vik-hallgatoi` - a BME VIK-es hallgatók csoportja 
 * `vik-oktatoi` - a BME-VIK-es oktatók csoportja 

### Erőforrás limitek
Lehetőség van korlátozni a felhasználók által elért erőforrások időbeni és maximális mértékét. Megszabhatunk csoport és felhasználói szintű megkötéseket is, a root felhasználóra semmilyen limitáció nem vonatkozik. 

Az erőforrások jelentős része un. Trackable RESources (TRES - nyomonkövethető erőforrások), például ilyen: CPU, Memória, Energia, GRES (Generic resource, pl. GPU). TRES-hez tartozik a kiszámlázott (Billing) költség, ez az előzőekből számolódik egy bizonyos súlyozás szerint. Ezekre is előírhetunk különböző limiteket. Például egyszerre egy felhasználónak hány aktív job-ja lehet, vagy kiszámlázott költsége nem érhet el valamekkora értéket, különben nem tud új job-ot futtatni.

A kiszámlázott (billing) költség az egyes partíciókon a következőképp alakul:

```
debug TRESBillingWeights="CPU=5.0,Mem=4.0G"
gpu   TRESBillingWeights="CPU=0.25,Mem=0.10G,gres/gpu=0.4"
htp   TRESBillingWeights="CPU=0.20,Mem=0.10G"

```

Például ha egy munkát indítunk 8 processzorral valamint 10GB memóriával a htp partíción, akkor a számlázott TRES költsége (TRES billing): `8*0.2 + 10GB*0.1 = 2.6`. Ha a job 2 óráig fut, akkor a számlázott TRES percek (TRES billing minutes): `120*2.6=312` 


A jelenleg telepített SLURM verzió és plugin-ok a következő limitációkat támogatja:
 * _GrpTRESMins_ - a felhasználható TRES percek, a lefutott, a futó és a jövőbeni job-ok alapján a használattól függően csökken
 * _GrpTRESRunMins_ - a futó job-ok maximális együttes TRES perce egy csoporton belül
 * _GrpTRES_ - bármely helyzetben maximálisan használható TRES költségek értéke (ha elérik a határt, a job várakozik) csoporton belül
 * _GrpJobs_ - az egyszerre futó job-ok maximális száma csoporton belül
 * _GrpSubmitJobs_ - a maximálisan beküldhető job-ok száma a várakozási sorba egy csoporton belül
 * _MaxTRESMinsPerJob_ - egy feladat által maximálisan felhasználható TRES percek értéke
 * _MaxTRESPerJob_ - egy feladat maximális TRES költsége

## ADMIN


## Információk a futó és lefutott Job-okról

Részletes információt kaphatunk a `sacct` parancs használatával. Egy példa:
``` 
sacct -a \
--format="jobid,jobname%6,user,account,state,totalcpu,elapsed,tres%80,nodelist%6,allocgres%6,alloctres%30" \
-S 2022-01-01-00:00:00
``` 
