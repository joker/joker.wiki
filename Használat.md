A használati sémát a BME szupergépén már bevált projekt bázison alakítottuk ki. Azaz a felhasználók projektekhez igényelhetnek hozzáférést. Egy projekt létrehozásakor a projektvezetőnek meg kell adni a projekt nevét, rövid leírását, célját, erőforrásigényét és várható futamidejét. A projektre a projekt nevével és megfelelő ssh kulccsal lehet belépni. A projektvezető ezután további projekttagokat adhat a projekthez. Minden projekttagnak önálló ssh kulccsal kell rendelkeznie.   
Interaktív belépés közvetlenül csak a fejgépre lehetséges a projektnévvel, mint azonosítóval és az egyedi ssh-kulccsal.

Új felhasználó esetén használathoz a [joker](https://joker.cloud.bme.hu/accounts/login/) honlapon lehet igényelni hozzáférést az adminoktól (eduid belépés), a következők megadásával:
```   
Projekt neve: 6-8 (karakter)
Projektvezető neve, címe
Projekt rövid leírása: 300-500 karakter.
Tervezett időtartama: 
Igényelt erőforrások (GPU, vCPU/node, diszk)
``` 
Az ssh kulcsokat a portálon regisztráció után meg lehet adni, így azt nem kell elküldeni. 

Ha valakinek van már futó, érvényes projektje kérjük regisztráljon a portálon a projekt nevének megadásával (ez azonos a linux felhasználó nevével). 

Ha valaki már aktív felhasználóként szeretne egy új projektet létrehozni, kérjük írjon nekünk: karsa at cloud.bme.hu vagy szebi at itt.bme.hu. Ekkor a korábbi projekt megmarad, mindkét projekthez szinkronizálódnak az sah-kulcsok. 

### [Belépés](Belépés)
### [SLURM használata](SLURM használata)
### [Speciális erőforrások - GPU](Speciális erőforrások - GPU)
### [Slurm-Jupyter](Slurm-Jupyter)
### [Hello Jupyter Notebook](Hello Jupyter Notebook)
### [Docker image futtatása](Docker image futtatása)
### [MPI-programok-futtatása](MPI-programok-futtatása)
### [Limitációk és accounting](Accounting)
### [Kvóta rendszer](Kvóta-rendszer)