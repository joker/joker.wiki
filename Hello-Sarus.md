Töltsünk le egy egyszerű rendszert (pl. BusyBox) tartalmazó docker image-t: 
```
$ sarus pull busybox
# image            : index.docker.io/library/busybox:latest
# cache directory  : "/home1/szebi/.sarus/cache"
# temp directory   : "/tmp"
# images directory : "/home1/szebi/.sarus/images"
> save image layers ...
> found in cache : sha256:e2334dd9fee4b77e48a8f2d793904118a3acf26f1f2e72a3d79c6cae993e07f0
> expanding image layers ...
> extracting     : "/home1/szebi/.sarus/cache/sha256:e2334dd9fee4b77e48a8f2d793904118a3acf26f1f2e72a3d79c6cae993e07f0.tar"
> make squashfs image: "/home1/szebi/.sarus/images/index.docker.io/library/busybox/latest.squashfs"
```

Ellenőrizzuk, hogy mi került a tárolónkba!
```
$ sarus images 
REPOSITORY      TAG               DIGEST         CREATED               SIZE         SERVER
busybox         latest            9fe025227098   2020-04-27T00:05:06   720.00KB     index.docker.io
```

Most indítsuk el a BusyBox-ot valamelyik workeren és írja ki, hogy "Hello Sarus"!
```
$ srun -p debug sarus run busybox echo "Hello Sarus"
Hello Sarus
```