Hozzáférés igénylése és SSH kulcs megadás, Jupyter notebook: [https://joker.cloud.bme.hu](https://joker.cloud.bme.hu)

SSH: {linux_user/projekt_név}@joker.cloud.bme.hu

A fejgépre történő első belépés után fontos kialakítani a projekt struktúráját, ami a projektvezető feladata. Különösen fontos a jó katalógusstruktúra kialakítása, ha többen dolgoznak együtt. A projekt HOME katalógusa minden esetben a /home1/PROJETKNEV.

Belépés után minden alkalommal fontos a környezeti változók megfelelő beállítása, amit a *module* parancs segít. A lehetőségek a `module avail` paranccsal listázhatók ki. Jelenleg az alábbi  környezetek tölthetők be:

- anaconda3 - Anaconda Individual Edition
- cuda - legmagasabb telepített CUDA változat  
- cuda10.2 - CUDA 10.2
- cuda10.0 - CUDA 10.0
- gcc-7 - CentOs devtoolset-7
- gcc-9 - CentOs devtoolset-9 
- sarus - sarus 1.1.0

A sarus környezet pl. a `module load sarus` paranccsal tölthető be és a `module unload sarus` paranccsal törölhető.  

Megj:  
Gyakran használt környezeteket célszerű a *.bashrc*-ből beállítani.  