A [Slurm-Jupyter](https://slurm-jupyter.readthedocs.io/en/latest/index.html) egy olyan kiegészítés, ami lehetővé teszi Jupyter Notebook futtatását SLURM által felügyelt klaszteren. A fenti linken található leírás alapvetően használható, érdemes átolvasni. A modult a fejgépre telepítettük, azon csak a beállításokra van szükség. 

# Webes megoldás

A webes felületen ([joker](https://joker.cloud.bme.hu:444)) a Jupyterhub segítségével az elérhető profilokból erőforrásokat allokálhatunk, majd ott elindíthatunk egy Jupyter szervert. A bejelentkezés jelenleg a joker web által biztosított oauth authentikációval történik. A következő teendőket kell elvégeznünk a használathoz:
 1. Lépjen be ssh-val a központi gépre. Törölje a `.jupyter` könyvtárat (saját home-ban), ha van: `rm -r .jupyter/`
 2. Lépjen be eduid azonosítóval a joker webes felületére: [https://joker.cloud.bme.hu](https://joker.cloud.bme.hu), a jobb felső sarokban válassza a jupyterhub-os ikont, majd válassza ki melyik linux felhasználó alatt szeretné használni a jupyter-t, fogadja el az authorizáációt.
 3. Válasszon az elérhető job profilokból és indítsa el a notebook-ot.

Ha problémába ütközik, kérem írjon: `karsa at cloud.bme.hu`

# Testreszabható konfig

Némi beállítást kell elvégeznünk a használathoz: 

## Beállítások a fejgépen (a joker szerveren bejelentkezve):
 1. ```cd``` 
 1. ```module load anaconda3```
 2. ```config-slurm-jupyter.sh```
 3. ```ln -s /usr/local/anaconda3 . ```
  
## Telepítés a felhasználó lokális környezetében
 1. Telepíteni kell az Anaconda python környezetet (Windows esetén is add hozzá a PATH-hoz, különben nem lesz egyszerű használni, csak egy segédprogramon keresztül, amit nem támogat jelenleg a slurm-os megoldás.)
 2. ```conda install -c kaspermunch slurm-jupyter```
 3. A kulcsok a fejgépre való belépéshez, már készen vannak ez a lépés kiesik. **Windows esetén az openssh-val kell kulcsot generálni, ne putty-al, különben a jupyter nem fog működni vele: [link](https://phoenixnap.com/kb/generate-ssh-key-windows-10) (csak az első része, az openssh kulcsgenerálás kell). Kérem ne adjon meg jelszót a kulcshoz!**

### A verzió
 - A.4. Kell egy kis patch, mert az installált változatba néhány dolog hardkódolva. Le kell tölteni a [patch](https://git.ik.bme.hu/joker/joker/blob/master/slurm-jupyter.patch) fájlt, majd
```
cd anaconda3
patch -p1 < letöltött_patch
```

### B verzió
_Ajánlott Windows használata mellett, de Linux esetén is könnyen használható. Windows alatt érdemes az Anaconda Prompt-ból indítani._
 - B.4. Töltsd le a git-ről az upgrade.py fájlt: [link](https://git.ik.bme.hu/joker/joker/blob/script/upgrade/upgrade.py)
 - B.5. Futtasd a fájlt, lehet rendszergazdai jogok kellenek! A script megkeresi az anaconda helyét és lecseréli a szükséges fájlokat.

__Ellenőrzött Anaconda verziók:__ `4.10.3`,

__Ellenőrzött Slurm-jupyter verziók:__ `2.0.22`,

### MacOS rendszer

Sajnos a fenti script nem teljesen jól működik, kérem kövesse a következő lépéseket:
 - keresse meg a slurm_jupyter modul helyét, alapesetben itt kell lennie: `/Users/<felhasználó>/opt/anaconda3/lib/python<verzió>/site-packages/slurm_jupyter`
 - módosítsa az `__init__.py` fájlt a következő módon:
 - cserélje le a következő sort: 
`cmd = 'ssh -L{port}:{node}.genomedk.net:{hostport} {user}@{frontend}'.format(**spec)` (open_port függvény) erre:
`cmd = 'ssh -L{port}:{node}:{hostport} {user}@{frontend}'.format(**spec)`
 - majd próbálja meg a lenti parancsokat kiadni


## Használat a lokális gépről:
_Ha az anaconda nincs automatikusan aktiválva: `conda activate`_
 1. A helpet érdemes megnézni:  ```slurm-jupyter -h```
 2. egy példa: itt helyileg a 1001-es porton érjük el a notebook-ot, míg a node-on a uid alapján kapunk szabad portot és 2Gb memóriát kérünk:

```
slurm-jupyter --run notebook -f joker.cloud.bme.hu -m 2g --port 10001 -u JOKER_USER -q {gpu|prod}
```   
 3. A felugró böngészőben, ha a https-re irányít át minket, kérjük a titkosítatlan http weboldalt.

Ennek egy lehetséges kimenete (a node a 2101 porton, míg helyileg a megadott 10001 porton kommunikálunk):

```
Submitted slurm with job id: 9689
Waiting for slurm job allocation
Compute node(s) allocated: cn02
Jupyter server: (to stop the server press Ctrl-C)
[I 11:39:09.241 NotebookApp] JupyterLab extension loaded from /usr/local/anaconda3/lib/python3.7/site-packages/jupyterlab
[I 11:39:09.241 NotebookApp] JupyterLab application directory is /usr/local/anaconda3/share/jupyter/lab
[I 11:39:09.244 NotebookApp] Serving notebooks from local directory: /home1/tlabor
[I 11:39:09.244 NotebookApp] The Jupyter Notebook is running at:
 Your browser may complain that the connection is not private.
 In Safari, you can proceed to allow this. In Chrome, you need"
 to simply type the characters "thisisunsafe" while in the Chrome window.
 Once ready, jupyter may ask for your cluster password.
[I 11:39:09.245 NotebookApp] https://cn02:2101/
[I 11:39:09.245 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[I 11:39:44.107 NotebookApp] 302 GET / (192.168.14.1) 1.60ms
```

Lehet, hogy bár megnyitja a böngészőben a lapot, az kezdetben nem tölt be, ekkor egy újratöltés segít. Vagy a https-re dob, ekkor a http-t kell kérni. A config-slurm-jupyter.sh-nál beálított jelszóval lehet belépni.

### Mire kell figyelni?
Ha elindítottuk a szervert a fenti paranccsal, használat után állítsuk is meg, ahogy írja: Ctrl+C és y, bár 1 nap után amúgy is kilőné a slurm, ne pazaroljunk!

Ha valamiért mégis ütközés van a portok miatt a node-on, a `--hostport` -al beállítható a távoli port is.

Lehetséges hogy nem sikerül azt a portot lefoglani, amit kértünk, mert egy másik szolgáltatás működik alatta (a `--hostport` távoli port már ki van osztva), pl.::

```
slurm-jupyter --run notebook -f joker.cloud.bme.hu -m 2g --port 10001 --hostport 10001 -u demouser -q gpu
Submitted slurm with job id: 9640 
Waiting for slurm job allocation
Compute node(s) allocated: cn02 
Jupyter server: (to stop the server press Ctrl-C)
[I 14:43:09.957 NotebookApp] The port 10001 is already in use, trying another port.
[I 14:43:10.015 NotebookApp] JupyterLab extension loaded from /usr/local/anaconda3/lib/python3.7/site-packages/jupyterlab
[I 14:43:10.015 NotebookApp] JupyterLab application directory is /usr/local/anaconda3/share/jupyter/lab
[I 14:43:10.018 NotebookApp] Serving notebooks from local directory: /home1/tlabor
[I 14:43:10.018 NotebookApp] The Jupyter Notebook is running at:
 Your browser may complain that the connection is not private.
 In Safari, you can proceed to allow this. In Chrome, you need"
 to simply type the characters "thisisunsafe" while in the Chrome window.
 Once ready, jupyter may ask for your cluster password.
[I 14:43:10.018 NotebookApp] https://cn02:10002/
[I 14:43:10.018 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
```

Ekkor nem tud betölteni az oldal, vagy lehet egy másik felhasználó, éppen aktív jupyter szerveréhez csatlakoztunk, ami jelszót kér és mi nem tudjuk persze. :) Fontos ugyanakkor, hogy ez esetben is le tudjuk állítani azt, hiszen csak az ssh tunneling rossz, maga az ssh kapcsolat nem, amivel vezéreljük a szervert. Ilyenkor állítsuk meg, és válasszunk egy másik hostportot.

Előfordulhat olyan eset is, amikor speciális portot kérünk (tipikusan 1023 alatt), ekkor valami hasonló üzenet fogad, mint a lenti esetben. Itt is a megoldás egy másik port választása.

```
[W 18:22:50.357 NotebookApp] Permission to listen on port 1002 denied.
[W 18:22:50.357 NotebookApp] Permission to listen on port 1003 denied.
[W 18:22:50.357 NotebookApp] Permission to listen on port 1004 denied.
[W 18:22:50.357 NotebookApp] Permission to listen on port 1005 denied.
[W 18:22:50.357 NotebookApp] Permission to listen on port 1006 denied.
```

## Notebook futtatása webes felület nélkül
Ha egy nagyobb notebook fájlt akarunk futtatni, ami akár több napig is fut, sajnos a fenti módszerrel nem nagyon lehetséges. Mivel a böngésző bezárásakor a futó notebook-ok megállnak. Ennek megoldására használható a fejgépen a `slurm-nb-run` parancs (használat elött be kell emelni az anaconda modult: `module load anaconda3`). Hasonlóan paraméterezhető, mint a slurm-jupyter. 

A lenti példa 3 Gb memóriával egy html kimenetet eredményez az indítás helyén:
```
slurm-nb-run -q gpu -m 3g --format html gyak1.ipynb
```

__Fontos:__ Csak akkor kapunk kimenetet, ha a notebook hiba nélkül lefutott, a hibát a saját könyvtárunk `.slurm_jupyter_run/` katalógusába írja `slurm_jupyter_run.{JOBID}.err` fájlba, ha minden rendben van, akkor a következőt kapjuk:

```
(base) {bm}[karsa@jokerdev .slurm_jupyter_run]$ cat slurm_jupyter_run.9743.err
[NbConvertApp] Converting notebook gyak1.ipynb to html
[NbConvertApp] Executing notebook with kernel: python3
[NbConvertApp] Writing 385295 bytes to gyak1.html
```

Érdemes a további opciókat megnézni: `slurm-nb-run --help`


Gyakorlati példa: [Hello Jupyter Notebook](Hello Jupyter Notebook)
 
 