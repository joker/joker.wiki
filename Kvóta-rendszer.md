## Felhasználói oldal
A `home1/` rendszeren fájlkvóta került bevezetésre felhasználónként. Minden felhasználónak meg van szabva a soft és hard korlátja. A soft limit 7 napig túlléphető (grace period) egészen a hard limitig. A saját diszkhasználatunkról és limitekről a `quota` paranccsal kaphatunk információt.

Példa soft limit túllépésre (warning), majd az írás is megszakad a hard limit elérésekor.
```
{bm}[karsazoltan@joker ~]$ dd if=/dev/zero of=bgfile bs=1M count=8
vdb: warning, user block quota exceeded.
vdb: write failed, user block limit reached.
dd: hiba ”bgfile” írása közben: Lemezkvóta túllépve
8+0 beolvasott rekord
7+0 kiírt rekord
7675904 bájt (7,7 MB) másolva, 0,0136829 mp, 561 MB/s
{bm}[karsazoltan@joker ~]$ quota
Disk quotas for user karsazoltan (uid 2509): 
     Filesystem  blocks   quota   limit   grace   files   quota   limit   grace
       /dev/vdb    7500*   5000    7500   6days       2       0       0    
```


## ADMIN
* Limitek beállítása (GB-ban): 
`setquota -u user [soft]G [hard]G [soft files] [hard files] [file sys]`
Példa: `setquota -u karsazoltan 5G 10G 0 0 /home1`
* Összes felhasználó reportja: `repquota -a`
* Összes `<soft/hard>` limit (KB) `repquota -a | awk '{split($0,a," "); sum += a[<4/5>]} END {print sum}'`


