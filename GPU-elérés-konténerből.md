Az alábbi példában egy Docker Hub-ról letöltött Ubuntu-val működő NVIDIA fejlesztőkörnyezetben futtatjuk le a nvidia-smi programot. Extra környezetként a konténer /usr/local/anaconda3 pontjára mountoljuk a /usr/local/anaconda3 könyvtárat. Itt jegyezzük meg, hogy a /home1/usernév könyvtár mindig csatolódik a konténerhez. Ez remekül kihasználható arra, hogy az itt elhelyezett adatok, szkriptek közvetlenül elérhetők a konténerből.
``` 
$ srun -p gpu --gres=mps sarus run \
   --mount=type=bind,source=/usr/local/anaconda3,destination=/usr/local/anaconda3 \
   --centralized-repository nvidia/cuda:10.2-devel-ubuntu18.04 nvidia-smi
```

**Megjegyzések:**

- Az nvidia/cuda:10.2-devel-ubuntu18.04 képfájl letöltöttük a központi tárolóba, így nem kell a felhasználói tárba betenni. A központi tár elérését a `--centralized-repository` kapcsoló írja elő.  
- A példa futtatása előtt ne felejtsük el beállítani a *sarus* használatához szükséges környezeti változókat (ld. belépés. `module load sarus`)! 

### Konténerben futó szkript + sbatch
Példaként készítsünk egy olyan SLURM szkriptet, ami egy konténerben futtat egy összetettebb feladatot. Ez utóbbi legyen egy Python program, ami Numba kódot futtat. A példához a korábban is használt Ubuntu konténert használjuk + /usr/local/anaconda3 alá telepített csomagokat, programokat.
A python szcript legyen a következő:
```
# cuda1.py 
from __future__ import division
from numba import cuda
import numpy
import math

# CUDA kernel
@cuda.jit
def my_kernel(io_array):
    pos = cuda.grid(1)
    if pos < io_array.size:
        io_array[pos] *= 2 # do the computation

# Host code
data = numpy.ones(256)
threadsperblock = 256
blockspergrid = math.ceil(data.shape[0] / threadsperblock)
my_kernel[blockspergrid, threadsperblock](data)
print(data)
```

Ezt egy segédszkripttel indítjuk el, ami beállítja a megfelelő környezeti változókat és meghívja a python interpretert:

```
#!/bin/bash
# cuda1.sh - elindítja a cuda1.py programot
# feltételezi, hogy a cuda toolkit a /usr/local/cuda-ban van és
# a python a /usr/local/anaconda3/bin-ben

PATH=/usr/local/anaconda3/bin:$PATH
CUDA_HOME=/usr/local/cuda

WDIR=`dirname $0`
python $WDIR/cuda1.py
```

Végül szükségünk van egy szkriptre, amit a SLURM hajt végre. Ez fogj elindítani a konténert és 
átdatja neki a paraméterként kapott szkriptet:
    
```
#!/bin/bash
# cuda1_run.sh - konténerben futtatja a paraméterként kapott szkriptet

#SBATCH -o cuda1.out
#SBATCH --gres=mps

module load sarus
sarus run --mount=type=bind,source=/usr/local/anaconda3,destination=/usr/local/anaconda3 \
          --centralized-repository nvidia/cuda:10.2-devel-ubuntu18.04 $PWD/$@
```

Feltételezzük, hogy mindhárom fájl abban a katalógusban van, ahonnan a jobot indítjuk a `sbatch -p gpu cuda1_run.sh cuda1.sh` paranccsal. 

ha minden rendben lefutott, akkor a `cuda1_out` fájl tartalmában találjuk a python szkript kimenetét:
```
$ cat cuda1.out
[2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2.
 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2.
 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2.
 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2.
 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2.
 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2.
 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2.
 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2.
 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2.
 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2.
 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2. 2.]

```
   

