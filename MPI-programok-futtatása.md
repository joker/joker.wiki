## MPI programok beállítása

A szerveren lehetőségünk van a MPI programokat futtatni, akár több node-on is. A használathoz a `module load mpi` parancs kiadása szükséges. A kommunikációs eszközt be kell állítani, ehhez a .bashrc-be kell rakni a `export UCX_NET_DEVICES=eth0` parancsot, vagy minden egyes belépésnél megadjuk. Ezután a megfelelő programokkal (_mpicc_, _mpirc++_, ...) tudjuk lefordítani a forrásfájlokat, majd az _mpirun_ paranccsal futtatni a programot a fejgépen. Például az alábbi paranccsal 4 darab folyamat indul el a main MPI programból:
```
mpirun -np 4 main
``` 

## Futtatás a SLURM ütemezővel
Mivel a fejgépen való használat korlátozott és nem is ajánlott, a SLURM ütemezéshez egy scriptet (_slurm.sh_) kell elkészíteni:
```
#!/bin/bash
#SBATCH --job-name=mpi       # a job neve
#SBATCH -N 4                 # hány node-ot szeretnénk használni
#SBATCH -p htp               # melyik partícióból
#SBATCH --ntasks-per-node=10 # egy node-on hány folyamat legyen (maximum 10)
#SBATCH --time=12:00:00      # maximális idő (a lsurm azután leállítja a futást)
#SBATCH -o slurm.out         # kimeneti fájl neve
# végrehajtandó mpi program
time mpirun main <paraméterek>
```
A _-N_ opcióval a node-ok (host-ok) számát adjuk meg, míg _--ntasks-per-node_ mondja meg hogy egy-egy node-on hány folyamat induljon el, így pl. a fenti példában 4*10=40 folyamat indul el a main programból. 

A scriptet ezután az _sbatch_ paranccsal ütemezzük:
```
{bm}[tlabor@joker oompi]$ sbatch slurm.sh 
Submitted batch job 11117
{bm}[tlabor@joker oompi]$ squeue
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON) 
             11117       htp      mpi   tlabor  R       0:03      4 cn[06-09] 
```

## C++11/14 környezet használata
A használathoz ki kell jelölni a szoftvereszközöket az `scl enable devtoolset-7 bash` paranccsal, így már lehet használni C++99 nagyobb verziókat. Segítségnek csináltam egy Makefile-t MPI C++14-hez:
```
CC = mpic++
CXXFLAGS = -Wall -std=c++14 -Wdeprecated -pedantic -g

HEADERS := $(wildcard *.h) $(wildcard *.hpp)
SOURCES := $(wildcard *.cpp)
OBJECTS := $(SOURCES:%.cpp=%.o)

.PHONY: main

main: $(OBJECTS)
        $(CC) $^ -g -o $@ -ldl
        
%.o: %.cpp $(HEADERS)
        $(CC) $(CXXFLAGS) -c $<

.PHONY: clean
clean:
        rm -rf $(OBJECTS) main
```