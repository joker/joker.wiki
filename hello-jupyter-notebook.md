# Jupyter Notebook Bevezető

## Kernelek

A jupyter notebook-ban a következő kernelek érhetők el:
 - C++ 
 - C++ thread
 - C++ OpenMP
 - python3
 - python3 CUDA
 - ROOT C++/python3

Egy-egy ilyen notebook tartalmazhat futtatandó kódot és annak kimenetét, valamint a kód leírását, megértését megkönnyítő markdown kiterjesztést. A notebook fájl html/pdf/latex... dokumentumba is kimenthető, így pl. egy beadandónál, nagyon könnyen felhasználható. Ez a doksi is ott készült. 

A _JOKER_ szerver érdekessége, hogy a Jupyter szervert nem a helyi gépen futtatjuk, hanem a _JOKER_ -en, így elérjük a _JOKER_ által biztosított erőforrásokat, mint pl. GPU számítási kapacitást is, bárhonnan. 

__Töltsd le a notebook fájlt, és indítsd el a saját joker környezetedben, majd haladj az ott leírt módon: [gyak1](https://git.ik.bme.hu/joker/joker/blob/script/gyak1/gyak1.ipynb)__

Ha a beállításokat még nem csináltad meg: [Slurm-Jupyter](Slurm-Jupyter)