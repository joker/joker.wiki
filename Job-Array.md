Az ún. *Job Array* segítségével komplexebb jobokat állíthatunk össze. Ez jól használható az ún. paraméter study feladatokban, amelyekben egy adott programot futtatunk több különböző paraméterrel. Legyen példaként a *hello_pstudy.sh* szkript, ami adott szavak előfordulási számát számolja meg egy szövegben: 
```
#!/bin/bash
# hello_pstuty.sh - "paraméter study" példa
#SBATCH -o hello_pstudy-%a.out
#SBATCH -a

szavak=(alma korte szilva barack meggy malna)
nr=$SLURM_ARRAY_TASK_ID
szo=${szavak[$nr]}
echo -n "$szo előfordulási száma: "
grep $szo adat.txt | wc -l
```

Indítsuk a szkriptet az `sbatch -p debug -a 0-5 hello_pstudy.sh` paranccsal. A job 6 taszkot generál, melyek a rendelkezésre álló erőforrástól függően egymás után, vagy egymással párhuzamosan futnak le. Eredményül 
6 fájl keletkezik (hello_pstudy-0.out, hello_pstudy-1.out, ... hello_pstudy-5.out) melyben az adott szó előfordulási száma szerepel.

