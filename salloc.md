#### Salloc 
Az `salloc` parancs lefoglalja a szükséges erőforrásokat, és elindítja a paraméterként megadott pogramot. Ha nem adunk paramétert, akkor az alapértelmezett shellt indítja el. Ellentétben a korábban megismert parancsokkal a `salloc` nem a lefoglalt erőforrások egyikén indítja el a megadott programot, hanem a submit gépen (estünkben fejépen). Ez lehetővé teszi, hogy a lefogallt erőforrásokat interaktívan kezeljük. Indtsuk el példaként a korábbi hello_slurm.sh szriptet: 
```
$ salloc -p debug -n 5 /bin/bash
salloc: Granted job allocation 423
$ /bin/hostname                 # Hol fut a bash? 
joker
$ printenv | grep SLURM         # Milyen környezeti változókat állított be? 
SLURM_NODELIST=cn01
SLURM_JOB_NAME=bash
SLURM_NODE_ALIASES=(null)
SLURM_NNODES=1
SLURM_JOBID=423
SLURM_NTASKS=10
SLURM_TASKS_PER_NODE=10
SLURM_CONF=/etc/slurm/slurm.conf
SLURM_JOB_ID=423
SLURM_SUBMIT_DIR=/home1/szebi/examples/hello_slurm
SLURM_NPROCS=10
SLURM_JOB_NODELIST=cn01
SLURM_CLUSTER_NAME=joker
SLURM_JOB_CPUS_PER_NODE=10
SLURM_SUBMIT_HOST=joker
SLURM_JOB_PARTITION=prod
SLURM_JOB_NUM_NODES=1
$ srun hello.sh                 # Elindítjuk az összes lefoglalt procin.    
Hello SLURM from:  cn01
Hello SLURM from:  cn01
Hello SLURM from:  cn01
Hello SLURM from:  cn01
Hello SLURM from:  cn01
$ srun -n3 hello.sh             # Most csak 3 proci kell.
Hello SLURM from:  cn01
Hello SLURM from:  cn01
Hello SLURM from:  cn01
$ srun -n6 hello.sh             # Most több kellene, nem fog menni. 
srun: error: Unable to create step for job 423: More processors requested than permitted
$ exit
salloc: Relinquishing job allocation 423
```

**FONTOS**, hogy ne felejtsünk el kilépni a shell-ből, hogy ne foglaljuk feleslegesen az erőforrásokat. Sajnos **nem egyszerű felismerni**, hogy melyik shell fut, mivel az a fejgépen fut, ahogy a login shell is, ezért könnyen eltévedhetünk.
Ezért ezt a lehetőséget (salloc) nagy figyelemmel kell használni, ha lehet kerüljük a használatát.   