A SLURM több lehetőséget ad a különböző erőforrások elérésére, igénylésére. Klaszterünkben a GRES (Generic Resource Scheduling) megoldást választottuk. Ennek segítségével két módon érhetők el a rendszerben levő 5 db GPU kártya: 

- kizárólagosan
- osztott módon

Ez utóbbi esetben egy GPU-t több felhasználó is használhat. Ezt a felhasználási módot javasoljuk használni a fejlesztéshez, hibakereséshez. Példaként futtassuk le az *nvidia-smi* programot az egyik osztott erőforráson:
```
$ srun -p gpu --gres=mps /usr/bin/nvidia-smi
```
Itt a `--gres=mps` a CUDA Multi-Process Service (MPS) használatát írja elő. Elképzelhető olyan feladat is, ahol az MPS lehetőségét kihasználva egy felhasználó több feladatot indít azonos GPU-n.

Amennyiben nem osztott GPU-n szeretnénk lefuttatni az adott programot, úgy a `--gres=gpu` erőforrást kell kérnünk. **Amennyiben nem** használjuk a `--gres=gpu` opciót, akkor a SLURM nem foglalja le a GPU-t. Adott esetben ezzel azt kockáztatjuk, hogy több jobb is megkapja az erőforrást.   

Várhatóan a **gpu**, mint erőforrás szűk keresztmetszet lesz a rendszereben. Ezért GPU-t használó programokat is batch fájlból érdemes futtatni. Példaként vegyünk egy egyszerű példaprogramot a CUDA fejlesztőkörnyezetből: 
Fontos, hogy ne felejtsük el a megfelelő környezeti változókat beállítani `module load cuda` paranccsal (ld. belépés). Ezután másoljuk le a példakódot, majd fordítsuk le:

```
$ cp -r /usr/local/cuda/samples/0_Simple/matrixMulCUBLAS/ .
$ cd matrixMulCUBLAS
$ make INCLUDES=-I/usr/local/cuda/samples/common/inc
```

Ezzel előállt a futtatható kód. (Az include megadására azért van szükség, mert most kiemeltük a példaprogramok hierarchiájából ezt az egy példát.) 

Készítsünk egy többször használható batch fájlt a futtatáshoz: 

```
#!/bin/bash 
# gpu_run.sh 

#SBATCH -o gpu_run.out
#SBATCH --gres=mps

srun $@    # srun-ra valójában nincs is szükség, mert csak 1 példányban akarjuk elindítani. 
```

Végül futtassunk s szkriptet az `sbatch -p gpu gpu_run ./matrixMulCUBLAS` paranccsal. 

 