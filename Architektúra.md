## HW Architektúra 

A fejgéphez kapcsolódó workerek száma igény szerint növelhető. 
Induló konfiguráció 5 GPU-s workert (cn01-cn05) és 5 GPU nélküli workert (cn06-cn10) tartalmaz.
A workerek jelenlegi konfigurációja: 40GB RAM, 10 vCPU, 30GB diszk, NVIDIA V100 PCI 16GB.

A fejgép jelenlegi konfigurációja: 16GB RAM, 6 vCPU, 170GB+1TB diszk. 

Mind a workerek és fejgép közötti, mind a fejgép és a külvilág közötti hálózati kapcsolat 1Gb sávszélességű. 

Szükség esetén a konfigurációk változtathatók bizonyos ésszerű korlátok között, mivel a befogadó felhőnek (Fured) más oktatási szerepe is van.  

## SW Architektúra

A gépek szoftver rendszere egységes. Minden gépen CentOs 7 operációs rendszer van, valamint a további kulcsfontosságú szoftverek: 
- Modules - environment kezelő rendszer (https://modules.readthedocs.io/en/latest/)
- SLURM - erőforrás allokáló és ütemező rendszer (https://slurm.schedmd.com/documentation.html)
- Sarus - konténerkezelő motor (https://sarus.readthedocs.io/en/stable/index.html)
- CUDA 10.0, 10.2 
- anaconda3
- igény esetén további szoftverek, melyek a /usr/local-ba telepítve minden workeren elérhetők.   

A fejgép /usr/local katalógusa (ro), valamint a /home1 katalógusa (rw) NFS-sel elérhető a workerekről.

Minden gépről elérhető egy GlusterFs fájlrendszer a `/gv0` néven. Ennek használata nem terhelik a felhő NFS szerverét. Mivel még nincs sok tapasztalatunk a **GlusterFS-sel**, elsősorban intenzíven használt (temp)fájlok használatára javasoljuk (`/gv0/tmp`) . Jelenleg ide másoltuk fel az `ImageNet-1k` fájlokat, amit jelenleg több projekt is használ. 
  