A SLURM parancsoknak számos opcionális paramétere/kapcsolója van (https://slurm.schedmd.com/pdfs/summary.pdf). Ezek közül a leggyakrabban használt paraméterek a következők:

```
-n n    - a futtatni kívánt taszkok száma/lefoglalni kívánt CPU-k száma
-N N    - node-ok száma (N = min[-max])
-o file - standard outputot tartalmazó fájl neve
-p name - partíció neve
-i file - standard inputot tartalmazó fájl neve
-t time - idő limit
-D dir  - munkakatalógus neve
--mail-type= - mikor küldjön levelet 
--mail-user= - kinek küldje
--mincpus=n  - CPU-k minimális száma
--ntasks-per-node=n - node-onkénti taszkszám 
--prolog=program - job futtatása előtt lefuttatandó program neve
```

Fontos egyszerűsítés, hogy a kapcsolók mindegyike speciális kommentként beépíthető az `sbatch` parancs szkriptjeibe. Az alábbi példában a `-n 5` és a `-o hello_run.out` kapcsolókat építettük be a szkriptbe.

```
#!/bin/bash
#
# hello_run.sh
#SBATCH -o hello_run.out
#SBATCH -n 5

srun ./hello.sh
```

Ezeket azonban a parancssorból megadott kapcsolóval felül lehet bírálni. Ez azt jelenti, hogy a fenti scriptet a `sbacth -n 3 hello_run.sh` paranccsal indítva 5 helyett 3 példányban fut le a *hello.sh* szkript. 
