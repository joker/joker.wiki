A fejgépre belépve `sinfo` parancs például az alábbi kimenetet adja:
```
PARTITION AVAIL  TIMELIMIT  NODES  STATE NODELIST
debug*       up 1-00:00:00      1  down* cn01
debug*       up 1-00:00:00      1   idle cn10
gpu*         up 7-00:00:00      4   mix cn[02-05]
htp*         up 7-00:00:00      4   idle cn[06-09]

```
A példában a **debug** elnevezésű partícióhoz 2 gép tartozik, a cn01, ami éppen *down*, és a cn10, ami pedig *idle* állapotban van. A többi gép a **gpu** és a **htp** partíció része. Az alapértelmezett partíció a **debug**. A cn02,cn03,cn04 és a cn05 *mix* állapota arra utal hogy a gépek összes erőforrása nincs lefoglalva, de valamennyi már le van foglalva a jobok részére.

A várakozási sor állapota az `squeue` paranccsal kérdezhető le pl:
```
$ squeue 
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
               576      gpu     sarus   dapgan  R   17:48:47      1 cn05
               577      gpu     sarus   dapgan  R   17:47:12      1 cn02
               578      gpu     sarus   dapgan  R   17:46:07      1 cn03
               579      gpu     sarus   dapgan  R    1:46:11      1 cn04
```
A listáról leolvasható hogy a lekérdezés pillanatában 4 job futott, amiből 3 már közel 18 órája.  

Indítsunk egy paralel jobot! A SLURM filozófiája szerint egy paralel job több azonos időben futó programból áll. Ezeknek nem feltétlenül kell együttműködniük. Pl: `srun -n5 /bin/hostname` parancs 
elindítja 5 CPU-n a /bin/hostname programot, mennyiben van 5 szabad CPU az alapértelmezett partícióban. Eredményként ehhez hasonló kimenet keletkezik:
```
cn10
cn10
cn10
cn10
cn10
```

A SLURM nem csak CPU-t tud allokálni, hanem, teljes node-ot (workert) is. Így a `srun -N2 /bin/hostname` parancs két node-on indítja el a programot ehhez hasonló kimenetet adva: 
```
cn01
cn10
``` 