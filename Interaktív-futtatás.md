
Amennyiben interaktívan akarjuk használni a workeren futó Docker image-t, létre kell hozni a megfeleő pseudo terminál kapcsolatot:
```
$ srun -p debug --pty sarus run -t busybox
/ $ ls
bin    dev    etc    home   home1  proc   root   sys    tmp    usr    var
/ $ 
```
