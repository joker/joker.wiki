A SLURM erőforrás allokáló és ütemező rendszer HPC környezetben igen elterjedt. Sok dokumentáció, példa található hozzá (pl: https://slurm.schedmd.com/documentation.html). Itt csak pár alapvető parancsot ismertetünk: 

- sinfó - az erőforrások állapotáról tájékoztat
- srun  - paralel job futatása 
- sbatch - batch szkript várakozó sorba állítása
- squeue - queue állapotának lekérdezése
- salloc - erőforrások  
- scancel - job törlése a várakozási sorból

A SLURM rendszerben az erőforrások ún. partíciókhoz rendelhetők. A partíciók nem csak az erőforráscsoportokat jelölik ki, hanem azok maximális használati idejét is. A SLURM rendszerben csak egy várakozási sor van, de a partíció-erőforrás összerendelés révén a várakozási sor is több részre bomlik. Jelenleg 3 partíciót definiáltunk:  

- htp - Hosszú futási idejű jobok száméra kialakított környezet.
- gpu - NVIDA kártyákat tartalmazó gépek. 
- debug - Fejlesztéshez használható max 1 napos futási idejű jobok számára kialakított környezet. Ez az alapértelmezett partíció.        

#### [Kezdő lépések](Kezdő lépések)
#### [Hello SLURM](Hello SLURM)
#### [Job Array](Job Array)
#### [Salloc](salloc)
#### [Kapcsolók](Kapcsolók)

